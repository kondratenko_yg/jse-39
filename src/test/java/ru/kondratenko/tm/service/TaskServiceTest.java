package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.TaskRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.TASKS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.TASKS_FILE_NAME_XML;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;

class TaskServiceTest {
    private TaskService taskService;
    private UserService userService;
    private TaskRepository taskRepository;
    private Task task;

    @BeforeEach
    void setUp() {
        taskRepository = Mockito.mock(TaskRepository.class);
        userService = Mockito.mock(UserService.class);
        userService.currentUser = new User("user1",hashMD5("123"), Role.ADMIN);
        task =  new Task("task1","desc",userService.currentUser.getId(),1L);
        taskService = TaskService.getInstance();
        ReflectionTestUtils.setField(taskService,"taskRepository",taskRepository);
        ReflectionTestUtils.setField(taskService,"userService",userService);

    }

    @Test
    void create() throws InterruptedException, TaskNotFoundException {
        assertNull(taskService.create(null));
        assertNull(taskService.create(""));
        assertNull(taskService.create("", ""));
        assertNull(taskService.create("", "",1L));
        when(taskRepository.create("task1","desc",1L,1L)).thenReturn(task);
        when(taskRepository.removeById(task.getId(),userService.currentUser.getId())).thenReturn(task);
        assertEquals(task,taskService.create("task1","desc",1L,1L));
        TimeUnit.SECONDS.sleep(63);
        when(taskService.create("task1")).thenReturn(task);
        taskService.create("task1");
        verify(taskRepository).create("task1");
        taskService.create("task1","desc");
        verify(taskRepository).create("task1","desc");
        taskService.create("task1","desc",1L);
        verify(taskRepository).create("task1","desc",1L);
        taskService.clear();
    }

    @Test
    void update() throws TaskNotFoundException {
        assertNull(taskService.update(null, "", "", null));
        assertNull(taskService.update(1L, "", "gfb", 2L));
        assertNull(taskService.update(3L, null, "", null));
        when(taskRepository.update(1L,"dt","gj",null)).thenReturn(task);
        assertEquals(task,taskService.update(1L,"dt","gj",null));
        when(taskRepository.update(1L,"dt","gj",null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.update(1L,"dt","gj",null));
    }

    @Test
    void findByIndex() throws TaskNotFoundException {
        when(taskRepository.findByIndex(-1,null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findByIndex(-1,null));
        when(taskRepository.findByIndex(0,null)).thenReturn(task);
        assertEquals(task,taskService.findByIndex(0,null));
    }

    @Test
    void findByName() throws TaskNotFoundException {
        assertNull(taskService.findByName(null, null));
        assertNull(taskService.findByName("", null));
        when(taskRepository.findByName("task2",null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findByName("task2",null));
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        when(taskRepository.findByName("task1",null)).thenReturn(tasks);
        assertEquals(tasks,taskService.findByName("task1",null));
    }

    @Test
    void findById() throws TaskNotFoundException {
        assertNull(taskService.findById(null, null));
        when(taskRepository.findById(2L,null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findById(2L,null));
        when(taskRepository.findById(1L,null)).thenReturn(task);
        assertEquals(task,taskService.findById(1L,null));
    }

    @Test
    void removeByIndex() throws TaskNotFoundException {
        when(taskRepository.removeByIndex(-1,null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeByIndex(-1,null));
        when(taskRepository.removeByIndex(0,null)).thenReturn(task);
        assertEquals(task,taskService.removeByIndex(0,null));
    }

    @Test
    void removeByName() throws TaskNotFoundException {
        assertNull(taskService.removeByName(null, null));
        assertNull(taskService.removeByName("", null));
        when(taskRepository.removeByName("task2",null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeByName("task2",null));
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        when(taskRepository.removeByName("task1",null)).thenReturn(tasks);
        assertEquals(tasks,taskService.removeByName("task1",null));
    }

    @Test
    void removeById() throws TaskNotFoundException {
        assertNull(taskService.removeById(null, null));
        when(taskRepository.removeById(2L,null)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeById(2L,null));
        when(taskRepository.removeById(1L,null)).thenReturn(task);
        assertEquals(task,taskService.removeById(1L,null));
    }

    @Test
    void findAll()  {
        assertEquals(new ArrayList<Task>(),taskService.findAll());
    }

    @Test
    void findAllByUserId()  {
        assertNull(taskService.findAllByUserId(null));
        assertEquals(new ArrayList<Task>(),taskService.findAllByUserId(1L));
    }

    @Test
    void saveUpload() throws IOException {
        when(taskRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, taskService.saveJSON("test"));
        when(taskRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, taskService.saveXML("test"));
        when(taskRepository.uploadJSON("test",Task.class)).thenReturn(0);
        assertEquals(0, taskService.uploadFromJSON("test"));
        when(taskRepository.uploadXML("test",Task.class)).thenReturn(0);
        assertEquals(0, taskService.uploadFromXML("test"));


        when(taskRepository.saveJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.saveJSON(TASKS_FILE_NAME_JSON));
        when(taskRepository.saveXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.saveXML(TASKS_FILE_NAME_XML));
        when(taskRepository.uploadJSON(TASKS_FILE_NAME_JSON,Task.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.uploadFromJSON(TASKS_FILE_NAME_JSON));
        when(taskRepository.uploadXML(TASKS_FILE_NAME_XML,Task.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.uploadFromXML(TASKS_FILE_NAME_XML));

    }

}