package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.USERS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.USERS_FILE_NAME_XML;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;

class UserServiceTest {
    private UserService userService;
    private UserRepository userRepository;
    private final User user =  new User("user1",hashMD5("123"), Role.ADMIN);

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = UserService.getInstance();
        ReflectionTestUtils.setField(userService,"userRepository",userRepository);
    }

    @Test
    void create() {
        assertNull(userService.create(null,null,null,null,null));
        assertNull(userService.create("",null,null,null,null));
        assertNull(userService.create("login",null,null,null,null));
        assertNull(userService.create("login","123",null,null,null));
        assertNull(userService.create("login","123","Ivan",null,null));
        assertNull(userService.create("login","123","Ivan","Ivanov",null));
        when(userService.create("login","123","Ivan","Ivanov", Role.ADMIN)).thenReturn(user);
        assertEquals(user,userService.create("login","123","Ivan","Ivanov", Role.ADMIN));
        userService.clear();
    }
    @Test
    void checkPassword() {
        assertTrue(userService.checkPassword(user,"123"));
    }

    @Test
    void findByLogin()  {
        assertNull(userService.findByLogin(null));
        assertNull(userService.findByLogin(""));
        when(userRepository.findByLogin("user1")).thenReturn(user);
        assertEquals(user,userService.findByLogin("user1"));
    }

    @Test
    void findById()  {
        assertNull(userService.findById(null));
        when(userRepository.findById(1L)).thenReturn(user);
        assertEquals(user,userService.findById(1L));
    }

    @Test
    void updateByLogin() {
        assertNull(userService.updateByLogin(null, "", "", null));
        assertNull(userService.updateByLogin("", "", "gfb", ""));
        assertNull(userService.updateByLogin("login", null, "gfb", ""));
        assertNull(userService.updateByLogin("login", "123", null, null));
        assertNull(userService.updateByLogin("login", "123", "faezf", null));
        when(userRepository.updateByLogin("login", hashMD5("123"), "faezf", "dsd")).thenReturn(user);
        assertEquals(user,userService.updateByLogin("login", "123", "faezf", "dsd"));
    }

    @Test
    void updateById() {
        assertNull(userService.updateById(null, "", "", null));
        assertNull(userService.updateById(1L, null, "gfb", ""));
        assertNull(userService.updateById(1L, "123", null, null));
        assertNull(userService.updateById(1L, "123", "faezf", null));
        when(userRepository.updateById(1L, hashMD5("123"), "faezf", "dsd")).thenReturn(user);
        assertEquals(user,userService.updateById(1L, "123", "faezf", "dsd"));
    }

    @Test
    void updatePasswordByLogin() {
        assertNull(userService.updatePasswordByLogin(null, ""));
        assertNull(userService.updatePasswordByLogin("", null));
        assertNull(userService.updatePasswordByLogin("login",  null));
        when(userRepository.updatePasswordByLogin("login", hashMD5("123"))).thenReturn(user);
        assertEquals(user,userService.updatePasswordByLogin("login", "123"));
    }

    @Test
    void updatePasswordById() {
        assertNull(userService.updatePasswordById(null, ""));
        assertNull(userService.updatePasswordById(1L,  null));
        when(userRepository.updatePasswordById(1L, hashMD5("123"))).thenReturn(user);
        assertEquals(user,userService.updatePasswordById(1L, "123"));
    }


    @Test
    void removeByLogin() {
        assertNull(userService.removeByLogin(null));
        assertNull(userService.removeByLogin(""));
        when(userRepository.removeByLogin("user1")).thenReturn(user);
        assertEquals(user,userService.removeByLogin("user1"));
    }

    @Test
    void removeById()  {
        assertNull(userService.removeById(null));
        when(userRepository.removeById(1L)).thenReturn(user);
        assertEquals(user,userService.removeById(1L));
    }

    @Test
    void findAll()  {
        assertEquals(new ArrayList<User>(),userService.findAll());
    }


    @Test
    void saveUpload() throws IOException {
        when(userRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, userService.saveJSON("test"));
        when(userRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, userService.saveXML("test"));
        when(userRepository.uploadJSON("test")).thenReturn(0);
        assertEquals(0, userService.uploadFromJSON("test"));
        when(userRepository.uploadXML("test")).thenReturn(0);
        assertEquals(0, userService.uploadFromXML("test"));


        when(userRepository.saveJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.saveJSON(USERS_FILE_NAME_JSON));
        when(userRepository.saveXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.saveXML(USERS_FILE_NAME_XML));
        when(userRepository.uploadJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.uploadFromJSON(USERS_FILE_NAME_JSON));
        when(userRepository.uploadXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.uploadFromXML(USERS_FILE_NAME_XML));

    }

    @Test
    void addCommandToHistory()  {
        assertEquals(-1,userService.addCommandToHistory(null));
        assertEquals(0,userService.addCommandToHistory("test"));
        for (int i = 0; i<=userService.historyLimit;i++){
            userService.addCommandToHistory("test"+i);
        }
        assertEquals(1,userService.addCommandToHistory("test"));
    }

}