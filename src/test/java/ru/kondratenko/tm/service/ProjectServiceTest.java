package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.PROJECTS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.PROJECTS_FILE_NAME_XML;

class ProjectServiceTest {
    private ProjectService projectService;
    private ProjectRepository projectRepository;
    private final Project project =  new Project("project1","desc",1L);

    @BeforeEach
    void setUp() {
        projectRepository = Mockito.mock(ProjectRepository.class);
        projectService = ProjectService.getInstance();
        ReflectionTestUtils.setField(projectService,"projectRepository",projectRepository);
    }

    @Test
    void create() {
        assertNull(projectService.create(null));
        assertNull(projectService.create(""));
        assertNull(projectService.create("", ""));
        assertNull(projectService.create("", "",1L));
        when(projectService.create("project1")).thenReturn(project);
        projectService.create("project1");
        verify(projectRepository).create("project1");
        projectService.create("project1","desc");
        verify(projectRepository).create("project1","desc");
        projectService.create("project1","desc",1L);
        verify(projectRepository).create("project1","desc",1L);
        projectService.clear();
    }

    @Test
    void update() throws ProjectNotFoundException {
        assertNull(projectService.update(null, "", "", null));
        assertNull(projectService.update(1L, "", "gfb", 2L));
        assertNull(projectService.update(3L, null, "", null));
        when(projectRepository.update(1L,"dt","gj",null)).thenReturn(project);
        assertEquals(project,projectService.update(1L,"dt","gj",null));
        when(projectRepository.update(1L,"dt","gj",null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.update(1L,"dt","gj",null));
    }

    @Test
    void findByIndex() throws ProjectNotFoundException {
        when(projectRepository.findByIndex(-1,null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findByIndex(-1,null));
        when(projectRepository.findByIndex(0,null)).thenReturn(project);
        assertEquals(project,projectService.findByIndex(0,null));
    }

    @Test
    void findByName() throws ProjectNotFoundException {
        assertNull(projectService.findByName(null, null));
        assertNull(projectService.findByName("", null));
        when(projectRepository.findByName("project2",null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findByName("project2",null));
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        when(projectRepository.findByName("project1",null)).thenReturn(projects);
        assertEquals(projects,projectService.findByName("project1",null));
    }

    @Test
    void findById() throws ProjectNotFoundException {
        assertNull(projectService.findById(null, null));
        when(projectRepository.findById(2L,null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findById(2L,null));
        when(projectRepository.findById(1L,null)).thenReturn(project);
        assertEquals(project,projectService.findById(1L,null));
    }

    @Test
    void removeByIndex() throws ProjectNotFoundException {
        when(projectRepository.removeByIndex(-1,null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeByIndex(-1,null));
        when(projectRepository.removeByIndex(0,null)).thenReturn(project);
        assertEquals(project,projectService.removeByIndex(0,null));
    }

    @Test
    void removeByName() throws ProjectNotFoundException {
        assertNull(projectService.removeByName(null, null));
        assertNull(projectService.removeByName("", null));
        when(projectRepository.removeByName("project2",null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeByName("project2",null));
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        when(projectRepository.removeByName("project1",null)).thenReturn(projects);
        assertEquals(projects,projectService.removeByName("project1",null));
    }

    @Test
    void removeById() throws ProjectNotFoundException {
        assertNull(projectService.removeById(null, null));
        when(projectRepository.removeById(2L,null)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeById(2L,null));
        when(projectRepository.removeById(1L,null)).thenReturn(project);
        assertEquals(project,projectService.removeById(1L,null));
    }

    @Test
    void findAll()  {
        assertEquals(new ArrayList<Project>(),projectService.findAll());
    }

    @Test
    void findAllByUserId()  {
        assertNull(projectService.findAllByUserId(null));
        assertEquals(new ArrayList<Project>(),projectService.findAllByUserId(1L));
    }

    @Test
    void saveUpload() throws IOException {
        when(projectRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, projectService.saveJSON("test"));
        when(projectRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, projectService.saveXML("test"));
        when(projectRepository.uploadJSON("test",Project.class)).thenReturn(0);
        assertEquals(0, projectService.uploadFromJSON("test"));
        when(projectRepository.uploadXML("test",Project.class)).thenReturn(0);
        assertEquals(0, projectService.uploadFromXML("test"));


        when(projectRepository.saveJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.saveJSON(PROJECTS_FILE_NAME_JSON));
        when(projectRepository.saveXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.saveXML(PROJECTS_FILE_NAME_XML));
        when(projectRepository.uploadJSON(PROJECTS_FILE_NAME_JSON,Project.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.uploadFromJSON(PROJECTS_FILE_NAME_JSON));
        when(projectRepository.uploadXML(PROJECTS_FILE_NAME_XML,Project.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.uploadFromXML(PROJECTS_FILE_NAME_XML));

    }

}