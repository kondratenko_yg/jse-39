package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;

class ListenerProjectImplTest {
    private ProjectService projectService = Mockito.mock(ProjectService.class);
    private ProjectTaskService projectTaskService = Mockito.mock(ProjectTaskService.class);
    private UserService userService= Mockito.mock(UserService.class);
    private ListenerProjectImpl listenerProjectImpl;
    private InputStream stdin = System.in;
    private Project project;
    private List<Project> projects;


    @BeforeEach
    void setUp() throws ProjectNotFoundException {
        userService.currentUser = new User("Test","123", Role.USER);
        projects = new ArrayList<>();
        project = new Project("project1","desc",userService.currentUser.getId());
        projects.add(project);
        final List<Task> tasks = new ArrayList<>();
        Task task = new Task("task1");
        tasks.add(task);

        when(projectService.findById(1L,userService.currentUser.getId())).thenReturn(project);
        when(projectService.findByIndex(0,userService.currentUser.getId())).thenReturn(project);
        when(projectService.findByName("project1",userService.currentUser.getId())).thenReturn(projects);

        when(projectService.findAllByUserId(userService.currentUser.getId())).thenReturn(projects);
        when(projectTaskService.findAllByProjectId(project.getId())).thenReturn(tasks);

        when(projectService.findById(2L,userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        when(projectService.findByIndex(1,userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        when(projectService.findByName("project2",userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);

        when(projectService.findById(3L,userService.currentUser.getId())).thenReturn(null);
        when(projectService.findByIndex(2,userService.currentUser.getId())).thenReturn(null);
        when(projectService.findByName("project3",userService.currentUser.getId())).thenReturn(null);
        when(projectService.findByIndex(-1,userService.currentUser.getId())).thenReturn(null);

        listenerProjectImpl = new ListenerProjectImpl();
        ReflectionTestUtils.setField(listenerProjectImpl,"projectService",projectService);
        ReflectionTestUtils.setField(listenerProjectImpl,"projectTaskService",projectTaskService);
        ReflectionTestUtils.setField(listenerProjectImpl,"userService",userService);
    }

    @Test
    void updateInvalidCommand() throws IOException, ProjectNotFoundException {
        assertEquals(-1,listenerProjectImpl.update("ergg",new Scanner(System.in)));
    }

    @Test
    void createProjectWrongProjectName() throws IOException, ProjectNotFoundException {
        String data = "1project\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_CREATE,new Scanner(System.in)));

        String dataOK = "project\ndescription\n";
        System.setIn(new ByteArrayInputStream(dataOK.getBytes()));
        assertEquals(0,listenerProjectImpl.createProject(new Scanner(System.in)));
        System.setIn(stdin);
    }


    @Test
    void clear() throws ProjectNotFoundException, IOException {
        assertEquals(0,listenerProjectImpl.update(PROJECT_CLEAR,new Scanner(System.in)));
        when(projectService.removeById(project.getId(), userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> listenerProjectImpl.clearProject());
        userService.currentUser = null;
        assertEquals(0,listenerProjectImpl.update(PROJECT_CLEAR,new Scanner(System.in)));
    }

    @Test
    void list() throws ProjectNotFoundException, IOException {
        assertEquals(0,listenerProjectImpl.update(PROJECT_LIST,new Scanner(System.in)));
        projects.clear();
        assertEquals(-1,listenerProjectImpl.listProject());
        userService.currentUser = null;
        assertEquals(-1,listenerProjectImpl.update(PROJECT_LIST,new Scanner(System.in)));
    }

    @Test
    void viewProjectByIndex() throws ProjectNotFoundException, IOException {
        String data1 = 1+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerProjectImpl.update(PROJECT_VIEW_BY_INDEX,new Scanner(System.in)));

        String data2 = 2+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));

        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.viewProjectByIndex(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void viewProjectById() throws ProjectNotFoundException, IOException {
        String data1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerProjectImpl.update(PROJECT_VIEW_BY_ID,new Scanner(System.in)));

        String data2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));

        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.viewProjectById(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void viewProjectByName() throws ProjectNotFoundException, IOException {
        String dataText1 = "project1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerProjectImpl.update(PROJECT_VIEW_BY_NAME,new Scanner(System.in)));

        String dataText2 = "project2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.viewProjectByName(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateProjectByIndex() throws ProjectNotFoundException, IOException {
        String dataText = "sd\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerProjectImpl.updateProjectByIndex(new Scanner(System.in)));

        String dataNotPositive = "-3\n";
        System.setIn(new ByteArrayInputStream(dataNotPositive.getBytes()));
        assertEquals(-1,listenerProjectImpl.updateProjectByIndex(new Scanner(System.in)));

        String data1 = "1\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerProjectImpl.updateProjectByIndex(new Scanner(System.in)));

        String data2 = "2\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(ProjectNotFoundException.class,()->
                listenerProjectImpl.updateProjectByIndex(new Scanner(System.in)));

        String dataTextWrongName = "1\n1newname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataTextWrongName.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_UPDATE_BY_INDEX,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateProjectById() throws ProjectNotFoundException, IOException {
        String dataText1 = 1L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerProjectImpl.updateProjectById(new Scanner(System.in)));

        String dataText1WrongName = 1L+"\n1newname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText1WrongName.getBytes()));
        assertEquals(-1,listenerProjectImpl.updateProjectById(new Scanner(System.in)));

        String dataText2 = 2L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.update(PROJECT_UPDATE_BY_ID,new Scanner(System.in)));

        String dataText3 = 3L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText3.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_UPDATE_BY_ID,new Scanner(System.in)));

        System.setIn(stdin);
    }

    @Test
    void removeProjectById() throws ProjectNotFoundException, IOException {
        String data1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        when(projectService.removeById(1L,userService.currentUser.getId())).thenReturn(project);
        assertEquals(0,listenerProjectImpl.removeProjectById(new Scanner(System.in)));

        when(projectService.removeById(2L,userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        String data2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.removeProjectById(new Scanner(System.in)));

        String dataText = "ed\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerProjectImpl.removeProjectById(new Scanner(System.in)));

        String data3 = 3L+"\n";
        when(projectService.removeById(3L,userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(data3.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_REMOVE_BY_ID,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeProjectByIndex() throws ProjectNotFoundException, IOException {
        String data1 = 1+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        when(projectService.removeByIndex(0,userService.currentUser.getId())).thenReturn(project);
        assertEquals(0,listenerProjectImpl.removeProjectByIndex(new Scanner(System.in)));

        when(projectService.removeByIndex(1,userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        String data2 = 2+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.removeProjectByIndex(new Scanner(System.in)));

        String dataText = "ed\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerProjectImpl.removeProjectByIndex(new Scanner(System.in)));

        String data3 = 3+"\n";
        when(projectService.removeByIndex(2,userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(data3.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_REMOVE_BY_INDEX,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeProjectByName() throws ProjectNotFoundException, IOException {
        String dataText1 = "project1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        when(projectService.removeByName("project1",userService.currentUser.getId())).thenReturn(projects);
        assertEquals(0,listenerProjectImpl.removeProjectByName(new Scanner(System.in)));

        when(projectService.removeByName("project2",userService.currentUser.getId())).thenThrow(ProjectNotFoundException.class);
        String dataText2 = "project2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.removeProjectByName(new Scanner(System.in)));

        String dataText3 ="project3\n";
        when(projectService.removeByName("project3",userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(dataText3.getBytes()));
        assertEquals(-1,listenerProjectImpl.update(PROJECT_REMOVE_BY_NAME,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerProjectImpl.saveXML(null));
        assertEquals(-1,listenerProjectImpl.saveXML(""));
        assertEquals(0,listenerProjectImpl.saveXML("test"));
        when(projectService.saveXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_TO_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerProjectImpl.saveJSON(null));
        assertEquals(-1,listenerProjectImpl.saveJSON(""));
        assertEquals(0,listenerProjectImpl.saveJSON("test"));
        when(projectService.saveJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_TO_FILE_JSON,new Scanner(System.in)));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerProjectImpl.uploadFromXML(null));
        assertEquals(-1,listenerProjectImpl.uploadFromXML(""));
        assertEquals(0,listenerProjectImpl.uploadFromXML("test"));
        when(projectService.uploadFromXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_FROM_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerProjectImpl.uploadFromJSON(null));
        assertEquals(-1,listenerProjectImpl.uploadFromJSON(""));
        assertEquals(0,listenerProjectImpl.uploadFromJSON("test"));
        when(projectService.uploadFromJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_FROM_FILE_JSON,new Scanner(System.in)));
    }

}