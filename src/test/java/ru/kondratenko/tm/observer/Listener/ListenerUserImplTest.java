package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.service.UserService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;

class ListenerUserImplTest {
    private ListenerUserImpl listenerUser;
    private UserService userService= Mockito.mock(UserService.class);
    private InputStream stdin = System.in;
    private final User user =  new User("user1",hashMD5("123"), Role.ADMIN);
    private List<User> users = new ArrayList<>();
    @BeforeEach
    void setUp() {
        userService.currentUser = user;
        users.add(user);
        listenerUser = new ListenerUserImpl();
        when(userService.findAll()).thenReturn(users);
        when(userService.findByLogin("user1")).thenReturn(user);
        when(userService.findByLogin("user2")).thenReturn(null);
        when(userService.findById(1L)).thenReturn(user);
        when(userService.findById(2L)).thenReturn(null);
        when(userService.checkPassword(Mockito.any(),Mockito.any())).thenCallRealMethod();        ReflectionTestUtils.setField(listenerUser,"userService",userService);
    }

    @Test
    void updateInvalidCommand() throws IOException {
        assertEquals(-1,listenerUser.update("ergg",new Scanner(System.in)));
    }

    @Test
    void displayHistory() throws IOException {
        assertEquals(-1,listenerUser.update(HISTORY,new Scanner(System.in)));
        userService.history = new ArrayDeque<>();
        userService.history.add("test");
        assertEquals(0,listenerUser.update(HISTORY,new Scanner(System.in)));
    }

    @Test
    void displayVersion() throws IOException {
        assertEquals(0,listenerUser.update(VERSION,new Scanner(System.in)));
    }

    @Test
    void displayAbout() throws IOException {
        assertEquals(0,listenerUser.update(ABOUT,new Scanner(System.in)));
    }

    @Test
    void displayHelp() throws IOException {
        assertEquals(0,listenerUser.update(HELP,new Scanner(System.in)));
    }

    @Test
    void displayExit() throws IOException {
        assertEquals(0,listenerUser.update(EXIT,new Scanner(System.in)));
    }

    @Test
    void registry() throws IOException {
        String data = "user1\n123";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(0,listenerUser.update(LOG_ON,new Scanner(System.in)));

        String dataWrongPassword = "user1\n1234";
        System.setIn(new ByteArrayInputStream(dataWrongPassword.getBytes()));
        assertEquals(-1,listenerUser.update(LOG_ON,new Scanner(System.in)));

        String dataOK = "user2\n123\n";
        System.setIn(new ByteArrayInputStream(dataOK.getBytes()));
        assertEquals(-1,listenerUser.registry(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void logOff() throws IOException {
        assertEquals(0,listenerUser.update(LOG_OFF,new Scanner(System.in)));
    }

    @Test
    void displayUserInfo() throws IOException {
        assertEquals(0,listenerUser.update(USER_INFO,new Scanner(System.in)));
    }

    @Test
    void updateUser() throws IOException {
        String data = "1234\nname\nfirstname\n,lastname\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(0,listenerUser.update(USER_UPDATE,new Scanner(System.in)));
        userService.currentUser = null;
        assertEquals(-1,listenerUser.update(USER_UPDATE,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updatePassword() throws IOException {
        String data = "123\n1234\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(0,listenerUser.update(USER_UPDATE_PASSWORD,new Scanner(System.in)));

        String dataWrong = "1234\n1234\n";
        System.setIn(new ByteArrayInputStream(dataWrong.getBytes()));
        assertEquals(-1,listenerUser.update(USER_UPDATE_PASSWORD,new Scanner(System.in)));

        userService.currentUser = null;
        assertEquals(-1,listenerUser.update(USER_UPDATE_PASSWORD,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updatePasswordByLogin() throws IOException {
        String data = "user1\n121234\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(0,listenerUser.update(USER_PASSWORD_UPDATE_BY_LOGIN,new Scanner(System.in)));

        String dataWrong = "user2\n123234\n";
        System.setIn(new ByteArrayInputStream(dataWrong.getBytes()));
        assertEquals(-1,listenerUser.update(USER_PASSWORD_UPDATE_BY_LOGIN,new Scanner(System.in)));

        System.setIn(stdin);
    }

    @Test
    void updatePasswordById() throws IOException {
        String data = 1L+"\n121234\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(0,listenerUser.update(USER_PASSWORD_UPDATE_BY_ID,new Scanner(System.in)));

        String dataWrong = 2L+"\n123234\n";
        System.setIn(new ByteArrayInputStream(dataWrong.getBytes()));
        assertEquals(-1,listenerUser.update(USER_PASSWORD_UPDATE_BY_ID,new Scanner(System.in)));

        System.setIn(stdin);
    }

    @Test
    void createUser() throws IOException {
        String data ="login\n1234\nfirstname\nlastname\n";
        when(userService.create("login","1234","firstname","lastname", Role.USER)).thenReturn(null);
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(-1,listenerUser.update(USER_CREATE,new Scanner(System.in)));

        String dataOK ="login1\n1234\nfirstname\nlastname\n";
        when(userService.create("login1","1234","firstname","lastname", Role.USER)).thenReturn(user);
        System.setIn(new ByteArrayInputStream(dataOK.getBytes()));
        assertEquals(0,listenerUser.update(USER_CREATE,new Scanner(System.in)));

        System.setIn(stdin);
    }

    @Test
    void clearUser() throws IOException {
        assertEquals(0,listenerUser.update(USER_CLEAR,new Scanner(System.in)));
    }

    @Test
    void listUser() throws IOException {
        assertEquals(0,listenerUser.update(USER_LIST,new Scanner(stdin)));
        users.clear();
        assertEquals(-1,listenerUser.listUser());
        userService.currentUser = null;
        assertEquals(-1,listenerUser.update(USER_LIST,new Scanner(System.in)));
    }

    @Test
    void viewUserByName()  throws IOException {
        String dataText1 = "user1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerUser.update(USER_VIEW_BY_LOGIN,new Scanner(System.in)));

        String dataText2 = "user2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertEquals(-1,listenerUser.update(USER_VIEW_BY_LOGIN,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateUserByLogin()  throws IOException {
        String dataText1 = "user1\n123\nqwerty\n1232344\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerUser.update(USER_UPDATE_BY_LOGIN,new Scanner(System.in)));

        String dataText2 = "user2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertEquals(-1,listenerUser.update(USER_UPDATE_BY_LOGIN,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateUserById()  throws IOException {
        String dataText1 = 1L+"\n123\nqwerty\n1232344\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerUser.update(USER_UPDATE_BY_ID,new Scanner(System.in)));

        String dataText2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertEquals(-1,listenerUser.update(USER_UPDATE_BY_ID,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeUserByLogin()  throws IOException {
        String dataText1 = "user1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        when(userService.removeByLogin("user1")).thenReturn(user);
        assertEquals(0,listenerUser.update(USER_REMOVE_BY_LOGIN,new Scanner(System.in)));

        String dataText2 = "user2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        when(userService.removeByLogin("user1")).thenReturn(null);
        assertEquals(-1,listenerUser.update(USER_REMOVE_BY_LOGIN,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeUserById()  throws IOException {
        String dataText1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        when(userService.removeById(1L)).thenReturn(user);
        assertEquals(0,listenerUser.update(USER_REMOVE_BY_ID,new Scanner(System.in)));

        String dataText2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        when(userService.removeById(2L)).thenReturn(null);
        assertEquals(-1,listenerUser.update(USER_REMOVE_BY_ID,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerUser.saveXML(null));
        assertEquals(-1,listenerUser.saveXML(""));
        assertEquals(0,listenerUser.saveXML("test"));
        when(userService.saveXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_TO_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerUser.saveJSON(null));
        assertEquals(-1,listenerUser.saveJSON(""));
        assertEquals(0,listenerUser.saveJSON("test"));
        when(userService.saveJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_TO_FILE_JSON,new Scanner(System.in)));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerUser.uploadFromXML(null));
        assertEquals(-1,listenerUser.uploadFromXML(""));
        assertEquals(0,listenerUser.uploadFromXML("test"));
        when(userService.uploadFromXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_FROM_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerUser.uploadFromJSON(null));
        assertEquals(-1,listenerUser.uploadFromJSON(""));
        assertEquals(0,listenerUser.uploadFromJSON("test"));
        when(userService.uploadFromJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_FROM_FILE_JSON,new Scanner(System.in)));
    }

}