package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;

class ListenerTaskImplTest {
    private TaskService taskService = Mockito.mock(TaskService.class);
    private UserService userService= Mockito.mock(UserService.class);
    private ProjectTaskService projectTaskService = Mockito.mock(ProjectTaskService.class);
    private ListenerTaskImpl listenerTaskImpl;
    private InputStream stdin = System.in;
    private Task task;
    private List<Task> tasks;


    @BeforeEach
    void setUp() throws TaskNotFoundException {
        userService.currentUser = new User("Test","123", Role.USER);
        tasks = new ArrayList<>();
        task = new Task("task1","desc",userService.currentUser.getId());
        tasks.add(task);

        when(taskService.findById(1L,userService.currentUser.getId())).thenReturn(task);
        when(taskService.findByIndex(0,userService.currentUser.getId())).thenReturn(task);
        when(taskService.findByName("task1",userService.currentUser.getId())).thenReturn(tasks);

        when(taskService.findAllByUserId(userService.currentUser.getId())).thenReturn(tasks);
        when(taskService.findAllByProjectId(1L)).thenReturn(tasks);

        when(taskService.findById(2L,userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        when(taskService.findByIndex(1,userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        when(taskService.findByName("task2",userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);

        when(taskService.findById(3L,userService.currentUser.getId())).thenReturn(null);
        when(taskService.findByIndex(2,userService.currentUser.getId())).thenReturn(null);
        when(taskService.findByName("task3",userService.currentUser.getId())).thenReturn(null);
        when(taskService.findByIndex(-1,userService.currentUser.getId())).thenReturn(null);

        listenerTaskImpl = new ListenerTaskImpl();
        ReflectionTestUtils.setField(listenerTaskImpl,"taskService",taskService);
        ReflectionTestUtils.setField(listenerTaskImpl,"userService",userService);
        ReflectionTestUtils.setField(listenerTaskImpl,"projectTaskService",projectTaskService);
    }

    @Test
    void updateInvalidCommand() throws IOException, TaskNotFoundException, ProjectNotFoundException {
        assertEquals(-1,listenerTaskImpl.update("ergg",new Scanner(System.in)));
    }

    @Test
    void createTaskName() throws ProjectNotFoundException, IOException, TaskNotFoundException {
        String data = "1task\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_CREATE,new Scanner(System.in)));

        String dataOK = "task\ndescription\n";
        System.setIn(new ByteArrayInputStream(dataOK.getBytes()));
        assertEquals(0,listenerTaskImpl.createTask(new Scanner(System.in)));
        System.setIn(stdin);
    }


    @Test
    void clear() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_CLEAR,new Scanner(System.in)));
        when(taskService.removeById(task.getId(), userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> listenerTaskImpl.clearTask());
        userService.currentUser = null;
        assertEquals(0,listenerTaskImpl.update(TASK_CLEAR,new Scanner(System.in)));
    }

    @Test
    void list() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_LIST,new Scanner(System.in)));
        tasks.clear();
        assertEquals(-1,listenerTaskImpl.listTask());
        userService.currentUser = null;
        assertEquals(-1,listenerTaskImpl.update(TASK_LIST,new Scanner(System.in)));
    }

    @Test
    void viewTaskByIndex() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String data1 = 1+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_VIEW_BY_INDEX,new Scanner(System.in)));

        String data2 = 2+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));

        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.viewTaskByIndex(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void viewTaskById() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String data1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_VIEW_BY_ID,new Scanner(System.in)));

        String data2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));

        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.viewTaskById(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void viewTaskByName() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String dataText1 = "task1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_VIEW_BY_NAME,new Scanner(System.in)));

        String dataText2 = "task2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.viewTaskByName(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateTaskByIndex() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String dataText = "sd\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerTaskImpl.updateTaskByIndex(new Scanner(System.in)));

        String dataNotPositive = "-3\n";
        System.setIn(new ByteArrayInputStream(dataNotPositive.getBytes()));
        assertEquals(-1,listenerTaskImpl.updateTaskByIndex(new Scanner(System.in)));

        String data1 = "1\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_UPDATE_BY_INDEX,new Scanner(System.in)));

        String data2 = "2\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(TaskNotFoundException.class,()->
                listenerTaskImpl.updateTaskByIndex(new Scanner(System.in)));

        String dataTextWrongName = "1\n1newname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataTextWrongName.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_UPDATE_BY_INDEX,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void updateTaskById() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String dataText1 = 1L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        assertEquals(0,listenerTaskImpl.updateTaskById(new Scanner(System.in)));

        String dataText1WrongName = 1L+"\n1newname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText1WrongName.getBytes()));
        assertEquals(-1,listenerTaskImpl.updateTaskById(new Scanner(System.in)));

        String dataText2 = 2L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.update(TASK_UPDATE_BY_ID,new Scanner(System.in)));

        String dataText3 = 3L+"\nnewname\nnewd\n";
        System.setIn(new ByteArrayInputStream(dataText3.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_UPDATE_BY_ID,new Scanner(System.in)));

        System.setIn(stdin);
    }

    @Test
    void removeTaskById() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String data1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        when(taskService.removeById(1L,userService.currentUser.getId())).thenReturn(task);
        assertEquals(0,listenerTaskImpl.removeTaskById(new Scanner(System.in)));

        when(taskService.removeById(2L,userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        String data2 = 2L+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.removeTaskById(new Scanner(System.in)));

        String dataText = "ed\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerTaskImpl.removeTaskById(new Scanner(System.in)));

        String data3 = 3L+"\n";
        when(taskService.removeById(3L,userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(data3.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_REMOVE_BY_ID,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeTaskByIndex() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String data1 = 1+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        when(taskService.removeByIndex(0,userService.currentUser.getId())).thenReturn(task);
        assertEquals(0,listenerTaskImpl.removeTaskByIndex(new Scanner(System.in)));

        when(taskService.removeByIndex(1,userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        String data2 = 2+"\n";
        System.setIn(new ByteArrayInputStream(data2.getBytes()));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.removeTaskByIndex(new Scanner(System.in)));

        String dataText = "ed\n";
        System.setIn(new ByteArrayInputStream(dataText.getBytes()));
        assertEquals(-1,listenerTaskImpl.removeTaskByIndex(new Scanner(System.in)));

        String data3 = 3+"\n";
        when(taskService.removeByIndex(2,userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(data3.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_REMOVE_BY_INDEX,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeTaskByName() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        String dataText1 = "task1\n";
        System.setIn(new ByteArrayInputStream(dataText1.getBytes()));
        when(taskService.removeByName("task1",userService.currentUser.getId())).thenReturn(tasks);
        assertEquals(0,listenerTaskImpl.removeTaskByName(new Scanner(System.in)));

        when(taskService.removeByName("task2",userService.currentUser.getId())).thenThrow(TaskNotFoundException.class);
        String dataText2 = "task2\n";
        System.setIn(new ByteArrayInputStream(dataText2.getBytes()));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.removeTaskByName(new Scanner(System.in)));

        String dataText3 ="task3\n";
        when(taskService.removeByName("task3",userService.currentUser.getId())).thenReturn(null);
        System.setIn(new ByteArrayInputStream(dataText3.getBytes()));
        assertEquals(-1,listenerTaskImpl.update(TASK_REMOVE_BY_NAME,new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerTaskImpl.saveXML(null));
        assertEquals(-1,listenerTaskImpl.saveXML(""));
        assertEquals(0,listenerTaskImpl.saveXML("test"));
        when(taskService.saveXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_TO_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerTaskImpl.saveJSON(null));
        assertEquals(-1,listenerTaskImpl.saveJSON(""));
        assertEquals(0,listenerTaskImpl.saveJSON("test"));
        when(taskService.saveJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_TO_FILE_JSON,new Scanner(System.in)));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerTaskImpl.uploadFromXML(null));
        assertEquals(-1,listenerTaskImpl.uploadFromXML(""));
        assertEquals(0,listenerTaskImpl.uploadFromXML("test"));
        when(taskService.uploadFromXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_FROM_FILE_XML,new Scanner(System.in)));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerTaskImpl.uploadFromJSON(null));
        assertEquals(-1,listenerTaskImpl.uploadFromJSON(""));
        assertEquals(0,listenerTaskImpl.uploadFromJSON("test"));
        when(taskService.uploadFromJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_FROM_FILE_JSON,new Scanner(System.in)));
    }

    @Test
    void listTaskByProjectId() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        String data1 = 1L+"\n";
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_LIST_BY_PROJECT_ID,new Scanner(System.in)));
        tasks.clear();
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(-1,listenerTaskImpl.listTaskByProjectId(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void removeTaskFromProjectByIds() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        String data1 = 1L+"\n"+1L;
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_REMOVE_FROM_PROJECT_BY_IDS,new Scanner(System.in)));

        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.removeTaskFromProjectByIds(new Scanner(System.in)));
        System.setIn(stdin);
    }

    @Test
    void addTaskToProjectByIds() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        String data1 = 1L+"\n"+1L;
        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.update(TASK_ADD_TO_PROJECT_BY_IDS,new Scanner(System.in)));

        System.setIn(new ByteArrayInputStream(data1.getBytes()));
        assertEquals(0,listenerTaskImpl.addTaskToProjectByIds(new Scanner(System.in)));
        System.setIn(stdin);
    }


}