package ru.kondratenko.tm.observer.Publisher;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.observer.Listener.ListenerProjectImpl;
import ru.kondratenko.tm.observer.Listener.ListenerTaskImpl;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.UserService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;

class PublisherImplTest {
    private static  PublisherImpl publisher;
    private static ListenerProjectImpl listenerProject;
    private static ListenerTaskImpl listenerTask;
    private UserService userService;
    private ProjectService projectService;
    private InputStream stdin = System.in;

    @BeforeEach
    void setUp() {
        userService = Mockito.mock(UserService.class);
        projectService = Mockito.mock(ProjectService.class);
        userService.currentUser = new User("Test","123", Role.USER);
        listenerProject = Mockito.mock(ListenerProjectImpl.class);
        listenerTask = Mockito.mock(ListenerTaskImpl.class);
        publisher = new PublisherImpl(userService);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
    }

    @Test
    void checkCommand()  {
        assertTrue(publisher.checkCommand(LOG_ON));
        assertFalse(publisher.checkCommand(null));
        assertFalse(publisher.checkCommand(""));
        assertFalse(publisher.checkCommand("sdc"));
        assertFalse(publisher.checkCommand( USER_REMOVE_BY_LOGIN));
        userService.currentUser = null;
        assertFalse(publisher.checkCommand( PROJECT_UPDATE_BY_INDEX));
    }

    @Test
    void  startProject() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        String data =  PROJECT_VIEW_BY_NAME+"\n"+EXIT;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);
        publisher.start(scanner);
        verify(listenerProject,Mockito.times(1)).update(PROJECT_VIEW_BY_NAME,scanner);
        System.setIn(stdin);
    }

    @Test
    void  startTask() throws ProjectNotFoundException, IOException, TaskNotFoundException {
        String data =  TASK_VIEW_BY_NAME+"\n"+EXIT;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);
        publisher.start(scanner);
        verify(listenerTask,Mockito.times(1)).update(TASK_VIEW_BY_NAME,scanner);
        System.setIn(stdin);
    }

    @Test
    void  startProjectNotFoundException() throws ProjectNotFoundException, IOException {
        String data =   PROJECT_UPDATE_BY_INDEX+"\n"+EXIT;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);
        when(listenerProject.update(PROJECT_UPDATE_BY_INDEX,scanner)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() ->publisher.start(scanner));
        System.setIn(stdin);
    }

    @Test
    void  startTaskNotFoundException() throws ProjectNotFoundException, IOException, TaskNotFoundException {
        String data =   TASK_UPDATE_BY_INDEX+"\n"+EXIT;
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);
        when(listenerTask.update(TASK_UPDATE_BY_INDEX,scanner)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() ->publisher.start(scanner));
        System.setIn(stdin);
    }

    @AfterAll
    static void delete(){
        publisher.deleteListener(listenerProject);
        publisher.deleteListener(listenerTask);
    }

}