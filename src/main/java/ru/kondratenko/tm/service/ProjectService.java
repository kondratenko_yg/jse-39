package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;

import java.io.IOException;
import java.util.List;

public  class ProjectService {

    private static volatile ProjectService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private ProjectService() {
    }

    public static ProjectService getInstance() {
        if (instance == null) {
            synchronized (ProjectService.class) {
                if (instance == null) {
                    instance = new ProjectService();
                }
            }
        }
        return instance;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.update(id, name, description, userId);
    }

    public Project findByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        return projectRepository.findByIndex(index, userId);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name, userId);
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.findById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) throws ProjectNotFoundException{
        return projectRepository.removeByIndex(index, userId);
    }

    public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.removeById(id, userId);
    }

    public List<Project> removeByName(final String name, final Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name, userId);
    }

    public void clear() {
        projectRepository.clear();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

    public int saveJSON(final String  fileName) throws IOException {
        return projectRepository.saveJSON(fileName);
    }

    public int saveXML(final String fileName) throws IOException {
        return projectRepository.saveXML(fileName);
    }

    public int uploadFromJSON(final String  fileName) throws IOException {
        return projectRepository. uploadJSON(fileName, Project.class);
    }

    public int uploadFromXML(final String  fileName) throws IOException {
        return projectRepository. uploadXML(fileName, Project.class);
    }
}
