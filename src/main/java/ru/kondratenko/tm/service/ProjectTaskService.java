package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.repository.TaskRepository;

import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final TaskRepository taskRepository=TaskRepository.getInstance();

    private static ProjectTaskService instance = null;

    private ProjectTaskService() {
    }

    public static ProjectTaskService getInstance(){
        if (instance == null )  instance = new ProjectTaskService();
        return instance;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project = projectRepository.findById(projectId,userId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId,userId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public List<Task> findAllByProjectId(Long projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }

    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
