package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import static ru.kondratenko.tm.util.HashUtil.hashMD5;

public class UserService  {

    private final UserRepository userRepository;

    public User currentUser;

    public Deque<String> history = new ArrayDeque<>();

    public int historyLimit = 10;

    private static volatile UserService instance = null;

    private UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    public static UserService getInstance(){
        if (instance == null ) {
            synchronized (UserService.class) {
                if (instance == null) {
                    instance = new UserService();
                }
            }
        }
        return instance;
    }

    public User create(
            String login, String password,
            String firstName, String lastName, Role role)
    {
        if (login == null||login.isEmpty()) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        if (role == null) return null;
        return userRepository.create(login, hashMD5(password), firstName, lastName, role);
    }

    public boolean checkPassword(final User user, final String password) {
        return user.getPassword().equals(hashMD5(password));
    }

    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User updateByLogin(String login, String password, String firstName, String lastName) {
        if (login == null||login.isEmpty()) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateByLogin(login, hashMD5(password), firstName, lastName);
    }

    public User updateById(Long id, String password, String firstName, String lastName) {
        if (id == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateById(id, hashMD5(password), firstName, lastName);
    }

    public User updatePasswordByLogin(String login, String password) {
        if (login == null||login.isEmpty()) return null;
        if (password == null) return null;
        return userRepository.updatePasswordByLogin(login, hashMD5(password));
    }

    public User updatePasswordById(Long id, String password) {
        if (id == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordById(id, hashMD5(password));
    }

    public User removeByLogin(String login) {
        if (login == null||login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public int saveJSON(final String  fileName) throws IOException {
       return userRepository.saveJSON(fileName);
    }

    public int saveXML(final String fileName) throws IOException {
         return userRepository.saveXML(fileName);
    }

    public int uploadFromJSON(final String  fileName) throws IOException {
        return userRepository.uploadJSON(fileName);
    }

    public int uploadFromXML(final String  fileName) throws IOException {
        return userRepository.uploadXML(fileName);
    }

    public int addCommandToHistory(String command){
        if(command == null) return -1;
        history.add(command);
        if (history.size() > historyLimit){
            history.remove();
            return 1;
        }
        return 0;
    }

}
