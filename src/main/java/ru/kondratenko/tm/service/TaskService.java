package ru.kondratenko.tm.service;

import org.apache.commons.lang3.time.DurationFormatUtils;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.TaskRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;


public class TaskService {

    private final TaskRepository taskRepository;

    private static volatile TaskService instance = null;

    private final UserService userService = UserService.getInstance();

    private TaskService() {
        this.taskRepository = TaskRepository.getInstance();
    }

    public static TaskService getInstance() {
        if (instance == null) {
            synchronized (TaskService.class) {
                if (instance == null) {
                    instance = new TaskService();
                }
            }
        }
        return instance;
    }

    public void startCountDeadLine(Task task)  {
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(),task.getDeadline());
            long secondsToCheck = (diff+1)/2;
            while(diff > 0){
                diff = ChronoUnit.SECONDS.between(LocalDateTime.now(),task.getDeadline());
                if(diff % secondsToCheck == 0){
                    System.out.println("Time for task " + task.getName() + " - " + DurationFormatUtils.formatDuration(TimeUnit.SECONDS.toMillis(diff), "**H:mm:ss**", true));
                    try {
                        if(secondsToCheck <= diff){
                            TimeUnit.SECONDS.sleep(secondsToCheck-1);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(secondsToCheck>3600){
                        secondsToCheck = secondsToCheck/4;
                    }
                    if(secondsToCheck>30){
                        secondsToCheck = secondsToCheck/2;
                    }

                }
            }
            return diff;
        });

        future.thenAccept(result -> {
            if(userService.currentUser != null){
            synchronized (findAllByUserId(userService.currentUser.getId())) {
                try {
                    removeById(task.getId(),task.getUserId());
                    System.out.println(task.getName()+" was removed.");
                } catch (TaskNotFoundException e) {
                    e.printStackTrace();
                }
            }
            }
        });

    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.create(name);
        startCountDeadLine(task);
        return task;
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.create(name,description);
        startCountDeadLine(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.create(name,description,userId);
        startCountDeadLine(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId, final Long time) {
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.create(name,description,userId,time);
        startCountDeadLine(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.update(id, name, description, userId);
    }

    public Task findByIndex(final int index, final Long userId) throws TaskNotFoundException {
        return taskRepository.findByIndex(index, userId);
    }

    public List<Task> findByName(final String name, final Long userId) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name, userId);
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.findById(id, userId);
    }

    public Task removeByIndex(final int index, final Long userId) throws TaskNotFoundException {

        return taskRepository.removeByIndex(index, userId);
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.removeById(id, userId);
    }

    public List<Task> removeByName(final String name, final Long userId) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name, userId);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByUserId(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public int saveJSON(final String fileName) throws IOException {
        return taskRepository.saveJSON(fileName);
    }

    public int saveXML(final String fileName) throws IOException {
        return taskRepository.saveXML(fileName);
    }

    public int uploadFromJSON(final String fileName) throws IOException {
        return taskRepository.uploadJSON(fileName, Task.class);
    }

    public int uploadFromXML(final String fileName) throws IOException {
        return taskRepository.uploadXML(fileName, Task.class);
    }

}
