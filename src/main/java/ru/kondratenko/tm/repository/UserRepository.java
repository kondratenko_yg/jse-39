package ru.kondratenko.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository  {
    private List<User> users = new ArrayList<>();

    private static volatile UserRepository instance = null;

    private UserRepository() {
    }

    public static UserRepository getInstance(){
        if (instance == null ) {
            synchronized (UserRepository .class) {
                if (instance == null) {
                    instance = new UserRepository();
                }
            }
        }
        return instance;
    }

    public User create(
            final String login, final String password,
            final String firstName, final String lastName, final Role role) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, firstName, lastName, role);
            users.add(user);
        }
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, role);
            users.add(user);
        }
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User updateByLogin(final String login, final String password, final String firstName, final String lastName) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User updatePasswordByLogin(final String login, final String password) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updatePasswordById(final Long id, final String password) {
        User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User updateById(final Long id, final String password, final String firstName, final String lastName) {
        User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User updateRoleById(final Long id, final Role role) {
        User user = findById(id);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User updateRoleByLogin(final String login, final Role role) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User removeById(final Long id) {
        User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

    public int saveJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, users);
        return 0;
    }

    public int saveXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File(fileName), users);
        return 0;
    }

    public int  uploadJSON(String fileName ) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        users.clear();
        users = objectMapper.readValue(file, new TypeReference<List<User>>(){});
        return 0;
    }

    public int  uploadXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, User.class);
        clear();
        users = xmlMapper.readValue(new File(fileName), type);
        return 0;
    }

}
