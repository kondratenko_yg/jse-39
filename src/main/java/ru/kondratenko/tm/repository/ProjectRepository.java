package ru.kondratenko.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> {

    private static volatile ProjectRepository instance = null;

    public static final Logger logger = LogManager.getLogger(ProjectRepository.class);

    private ProjectRepository() {
    }

    public static ProjectRepository getInstance(){
        if (instance == null ) {
            synchronized (ProjectRepository .class) {
                if (instance == null) {
                    instance = new ProjectRepository();
                }
            }
        }
        return instance;
    }

    public String getName(Project project) {
        if (project == null) return null;
        return project.getName();
    }

    public Long getUserId(Project project) {
        if (project == null) return null;
        return project.getUserId();
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        items.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        items.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description, userId);
        items.add(project);
        addToMap(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description, final Long userId) throws ProjectNotFoundException {
        logger.trace("update -- > id: {}, name : {}, description: {}, userId: {}",id,name,description,userId);
        Project project = findById(id, userId);
        if (project == null) return null;
        String oldName = project.getName();
        List<Project> projectListOld = findByName(oldName, null);
        if (projectListOld == null) return null;
        project.setName(name);
        project.setDescription(description);
        if (!oldName.equals(name)) {
            projectListOld.remove(project);
            if (projectListOld.size() == 0) itemsA.remove(oldName);
            addToMap(project);
        }
        return project;
    }

    public Project findByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        List<Project> result;
        if (userId == null) result = findAll();
        else result = findAllByUserId(userId);
        if (result == null || result.size() == 0) {
            throw new ProjectNotFoundException("Project is not found by index " + index + ".");
        }
        if (index < 0 || index > result.size() - 1) throw new ProjectNotFoundException("Wrong index.");
        return result.get(index);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException {
        if (!itemsA.containsKey(name))
            throw new ProjectNotFoundException("Project are not found by name " + name + ".");
        List<Project> result = new ArrayList<>();
        if (userId == null) {
            result = itemsA.get(name);
        } else {
            for (Project project : itemsA.get(name)) {
                if (project.getUserId().equals(userId)) result.add(project);
            }
        }
        if (result == null || result.size() == 0) {
            throw new ProjectNotFoundException("Projects are not found by name " + name + ".");
        }
        return result;
    }

    public Project findById(final Long id, final Long userId) throws ProjectNotFoundException {
        List<Project> currentListTask;
        if (userId == null) currentListTask = findAll();
        else currentListTask = findAllByUserId(userId);
        if (currentListTask == null || currentListTask.size() == 0) {
            throw new ProjectNotFoundException("Project is not found by " + id + ".");
        }
        for (final Project project : currentListTask) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        logger.trace("removeByIndex -- > index: {}, userId: {}",index,userId);
        Project project = findByIndex(index, userId);
        if (project == null) return null;
        items.remove(project);
        List<Project> projectFromName = findByName(project.getName(), null);
        projectFromName.remove(project);
        return project;
    }

    public Project removeById(final Long id, final Long userId) throws ProjectNotFoundException {
        logger.trace("removeById -- > id: {}, userId: {}",id,userId);
        Project project = findById(id, userId);
        if (project == null) return null;
        items.remove(project);
        List<Project> projectFromName = findByName(project.getName(), null);
        projectFromName.remove(project);
        return project;
    }

    public List<Project> removeByName(final String name, final Long userId) throws ProjectNotFoundException {
        logger.trace("findByName -- > name: {}, userId: {}",name,userId);
        List<Project> projectList = findByName(name, userId);
        if (projectList == null || projectList.size() == 0) return null;
        List<Project> projectListFromName = findByName(name, null);
        items.removeAll(projectList);
        projectListFromName.removeAll(projectList);
        return projectList;
    }



}
