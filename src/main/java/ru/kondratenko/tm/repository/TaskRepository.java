package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task>  {

    private static volatile TaskRepository instance = null;

    private TaskRepository() {
    }

    public static TaskRepository getInstance(){
        if (instance == null ) {
            synchronized (TaskRepository .class) {
                if (instance == null) {
                    instance = new TaskRepository();
                }
            }
        }
        return instance;
    }

    public String getName(Task task) {
        if (task == null) return null;
        return task.getName();
    }

    public Long getUserId(Task task) {
        if (task == null) return null;
        return task.getUserId();
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        items.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        items.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description, userId);
        items.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId, final Long time) {
        final Task task = new Task(name, description, userId,time);
        items.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException {
        Task task = findById(id, userId);
        if (task == null) return null;
        String oldName = task.getName();
        List<Task> taskListOld = findByName(oldName, null);
        if (taskListOld == null) return null;
        task.setName(name);
        task.setDescription(description);
        if (!oldName.equals(name)) {
            taskListOld.remove(task);
            if (taskListOld.size() == 0) itemsA.remove(oldName);
            addToMap(task);
        }
        return task;
    }


    public Task findByIndex(int index, final Long userId) throws TaskNotFoundException {
        List<Task> result;
        if (userId == null) result = findAll();
        else result = findAllByUserId(userId);
        if (result == null || result.size() == 0) {
            throw new TaskNotFoundException("Task is not found by index " + index + ".");
        }
        if (index < 0 || index > result.size() - 1) throw new TaskNotFoundException("Wrong index.");
        return result.get(index);
    }

    public List<Task> findByName(final String name, final Long userId) throws TaskNotFoundException {
        if (!itemsA.containsKey(name)) throw new TaskNotFoundException("Tasks are not found by name " + name + ".");
        List<Task> result = new ArrayList<>();
        if (userId == null) {
            result = itemsA.get(name);
        } else {
            for (Task task : itemsA.get(name)) {
                if (task.getUserId().equals(userId)) result.add(task);
            }
        }
        if (result == null || result.size() == 0) {
            throw new TaskNotFoundException("Tasks are not found by name " + name + ".");
        }
        return result;
    }

    public Task findById(final Long id, final Long userId) throws TaskNotFoundException {
        List<Task> currentListTask;
        if (userId == null) currentListTask = findAll();
        else currentListTask = findAllByUserId(userId);
        if (currentListTask == null || currentListTask.size() == 0) {
            throw new TaskNotFoundException("Task is not found by id " + id + ".");
        }
        for (final Task task : currentListTask) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : items) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByIndex(final int index, final Long userId) throws TaskNotFoundException {
        Task task = findByIndex(index, userId);
        if (task == null) return null;
        items.remove(task);
        List<Task> taskFromName = findByName(task.getName(), null);
        taskFromName.remove(task);
        return task;
    }

    public Task removeById(final Long id, final Long userId) throws TaskNotFoundException {
        Task task = findById(id, userId);
        if (task == null) return null;
        items.remove(task);
        List<Task> taskFromName = findByName(task.getName(), null);
        taskFromName.remove(task);
        return task;
    }

    public List<Task> removeByName(final String name, final Long userId) throws TaskNotFoundException {
        List<Task> taskList = findByName(name, userId);
        if (taskList == null || taskList.size() == 0) return null;
        List<Task> taskListFromName = findByName(name, null);
        taskListFromName.removeAll(taskList);
        items.removeAll(taskList);
        return taskList;
    }


    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long IdUser = task.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(task);
        }
        return result;
    }

}
