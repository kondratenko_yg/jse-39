package ru.kondratenko.tm.observer.Publisher;

import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.observer.Listener.Listener;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static ru.kondratenko.tm.constant.TerminalConst.ALL_ACTIONS;
import static ru.kondratenko.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher {
    private  List<Listener> listeners ;
    private final UserService userService;

    public PublisherImpl(UserService userService) {
        this.listeners = new ArrayList<>();
        this.userService = userService;
    }

    @Override
    public void start(Scanner scanner) throws ProjectNotFoundException, TaskNotFoundException, IOException {
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            if(!checkCommand(command)) {continue;}
            userService.addCommandToHistory(command);
            notify(command,scanner);
            System.out.println();
        }
    }


    public boolean checkCommand(String command){
        if (command == null || command.isEmpty()) return false;
        if (!Arrays.asList(ALL_ACTIONS).contains(command)) {
            if (!Arrays.asList(Role.ADMIN.getActions()).contains(command)) {
                System.out.println("[FAIL]");
                return false;
            }
            if (userService.currentUser == null ) {
                System.out.println("[PLEASE, LOG ON OR REGISTRY.]");
                return false;
            }
            if (!Arrays.asList(userService.currentUser.getRole().getActions()).contains(command)) {
                System.out.println("[NOT ALLOWED.]");
                return false;
            }
        }
        return true;
    }

    @Override
    public void addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notify(String command,Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException {
        for (Listener listener : listeners){
            listener.update(command,scanner);
        }
    }
}
