package ru.kondratenko.tm.observer.Publisher;

import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.observer.Listener.Listener;

import java.io.IOException;
import java.util.Scanner;

public interface Publisher {
    void addListener(Listener listener);
    void deleteListener(Listener listener);
    void notify(String command, Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException;

    void start(Scanner scanner) throws ProjectNotFoundException, IOException, TaskNotFoundException;
}
