package ru.kondratenko.tm.observer.Listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import static ru.kondratenko.tm.constant.TerminalConst.*;

public class ListenerUserImpl  implements Listener {
    private final UserService userService = UserService.getInstance();
    protected final Logger logger = LogManager.getLogger(ListenerUserImpl.class);
    @Override
    public int update(String param, Scanner scanner) throws IOException {
        switch (param) {
            case HISTORY:
                return displayHistory();
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();
            case LOG_ON:
                return registry(scanner);
            case LOG_OFF:
                return logOff();
            case USER_INFO:
                return displayUserInfo();
            case USER_UPDATE:
                return updateUser(scanner);
            case USER_UPDATE_PASSWORD:
                return updatePassword(scanner);
            case USER_PASSWORD_UPDATE_BY_LOGIN:
                return updatePasswordByLogin(scanner);
            case USER_PASSWORD_UPDATE_BY_ID:
                return updatePasswordById(scanner);
            case USER_CREATE:
                return createUser(scanner);
            case USER_CLEAR:
                return clearUser();
            case USER_LIST:
                return listUser();
            case USER_VIEW_BY_LOGIN:
                return viewUserByLogin(scanner);
            case USER_UPDATE_BY_LOGIN:
                return updateUserByLogin(scanner);
            case USER_UPDATE_BY_ID:
                return updateUserById(scanner);
            case USER_REMOVE_BY_LOGIN:
                return removeUserByLogin(scanner);
            case USER_REMOVE_BY_ID:
                return removeUserById(scanner);
            case USERS_TO_FILE_XML:
                return saveXML(USERS_FILE_NAME_XML);
            case USERS_TO_FILE_JSON:
                return saveJSON(USERS_FILE_NAME_JSON);
            case USERS_FROM_FILE_XML:
                return uploadFromXML(USERS_FILE_NAME_XML);
            case USERS_FROM_FILE_JSON:
                return uploadFromJSON(USERS_FILE_NAME_JSON);
            default:
                return -1;
        }

    }

    public int displayHistory(){
        if (userService.history == null || userService.history.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return -1;
        }
        int index = 1;
        for (final String command : userService.history) {
            System.out.println(index + ". " + command);
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int displayExit() {
        System.out.println("Terminate console application...");
        return 0;
    }

    public int displayHelp() {
        System.out.println("log-on - Log on.");
        System.out.println("log-off - Log off.");
        System.out.println("user-info - User information.");
        System.out.println("user-update - Update user information.");
        System.out.println("user-update-password - Update current user password.");
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println("history - History of commands.");
        System.out.println();
        System.out.println("project-create - Create project.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println();
        System.out.println("task-create - Create task.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        System.out.println("user-create - Create user.");
        System.out.println("user-clear - Clear list of users.");
        System.out.println("user-list - Display list of users.");
        System.out.println("user-view-by-login - Display user.");
        System.out.println("user-remove-by-login - Remove user bu login.");
        System.out.println("user-update-by-login - Update user by login.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Kondratenko Iulia");
        System.out.println("kondratenko_yg@nlmk.com");
        return 0;
    }

    public int createUser(Scanner scanner) {
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        System.out.println("[PLEASE, ENTER password:]");
        final String password = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME:]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER LAST NAME:]");
        final String lastName = scanner.nextLine();

        if (userService.create(login, password, firstName, lastName, Role.USER) == null) {
            logger.info("Choose different login.");
            return -1;
        } else {
            logger.info("User is created.");
        }
        return 0;
    }

    public int updateUserByLogin(Scanner scanner) {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            logger.info("User is not found.");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateByLogin(login, password, firstName, lastName);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updateUserById(Scanner scanner) {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final User user = userService.findById(Listener.inputIdCheckFormat(scanner.nextLine()));
        if (user == null) {
            logger.info("User is not found.");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateById(user.getId(), password, firstName, lastName);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updatePasswordById(Scanner scanner) {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final User user = userService.findById(Listener.inputIdCheckFormat(scanner.nextLine()));
        if (user == null) {
            logger.info("User is not found.");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            userService.updatePasswordById(user.getId(), password);
            logger.info("User is updated.");
        }
        return 0;
    }

    public int updatePasswordByLogin(Scanner scanner) {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            logger.info("User is not found.");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            userService.updatePasswordByLogin(login, password);
            logger.info("Password is updated.");
        }
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        logger.info("User was deleted.");
        return 0;
    }

    public int removeUserByLogin(Scanner scanner) {
        System.out.println("[CLEAR USER BY LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null){
            logger.info("User was not deleted by login.");
            return -1;
        }
        logger.info("User was deleted by login.");
        return 0;
    }

    public int removeUserById(Scanner scanner) {
        System.out.println("[CLEAR USER BY ID]");
        System.out.println("ENTER ID: ");
        final User user = userService.removeById(Listener.inputIdCheckFormat(scanner.nextLine()));
        if (user == null){
            logger.info("User was not deleted by id.");
            return -1;
        }
        logger.info("User was deleted by id.");
        return 0;
    }

    public int viewUser(final User user) {
        if (user == null) return -1;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
        System.out.println("[OK]");
        return 0;
    }

    public int viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) {
            logger.info("User are not found.");
            return -1;
        }
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        return viewUsers(userService.findAll());
    }

    public int viewUserByLogin(Scanner scanner) {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        return  viewUser(user);
    }

    public int displayUserInfo() {
        viewUser(userService.currentUser);
        return 0;
    }

    public int registry(Scanner scanner) {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        if (user == null) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM.");
            return -1;
        }
        System.out.println("ENTER PASSWORD: ");
        if (userService.checkPassword(user, scanner.nextLine())) {
            System.out.println("WELCOME, " + user.getLogin());
            userService.currentUser = user;
        } else {
            System.out.println("FAIL");
            return -1;
        }
        return 0;
    }

    public int logOff() {
        userService.currentUser = null;
        System.out.println("LOG OFF");
        return 0;
    }

    public int updatePassword(Scanner scanner) {
        System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER OLD PASSWORD:]");
            final String oldPassword = scanner.nextLine();
            if (userService.checkPassword(userService.currentUser, oldPassword)) {
                System.out.println("[PLEASE, ENTER NEW PASSWORD:]");
                final String newPassword = scanner.nextLine();
                userService.updatePasswordByLogin(userService.currentUser.getLogin(), newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[WRONG PASSWORD]");
                return -1;
            }
        }
        return 0;
    }

    public int updateUser(Scanner scanner) {
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
            return -1;
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateById(userService.currentUser.getId(), password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int saveJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        userService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        userService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        userService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        userService.uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

}
