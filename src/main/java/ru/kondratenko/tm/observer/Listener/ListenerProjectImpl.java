package ru.kondratenko.tm.observer.Listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static ru.kondratenko.tm.constant.TerminalConst.*;

public class ListenerProjectImpl implements Listener  {
    private final ProjectService projectService;
    private final UserService userService;
    private final ProjectTaskService projectTaskService;
    protected final Logger logger;

    public ListenerProjectImpl() {
        this.projectService = ProjectService.getInstance();
        this.userService = UserService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
        this.logger = LogManager.getLogger(ListenerProjectImpl.class);
    }

    @Override
    public int update(String param,Scanner scanner) throws ProjectNotFoundException, IOException {
        switch (param) {
            case PROJECT_CREATE:
                return createProject(scanner);
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW_BY_INDEX:
               return viewProjectByIndex(scanner);
            case PROJECT_VIEW_BY_ID:
               return viewProjectById(scanner);
            case PROJECT_VIEW_BY_NAME:
               return viewProjectByName(scanner);
            case PROJECT_REMOVE_BY_INDEX:
               return removeProjectByIndex(scanner);
            case PROJECT_REMOVE_BY_ID:
               return removeProjectById(scanner);
            case PROJECT_REMOVE_BY_NAME:
               return removeProjectByName(scanner);
            case PROJECT_UPDATE_BY_INDEX:
               return updateProjectByIndex(scanner);
            case PROJECT_UPDATE_BY_ID:
               return updateProjectById(scanner);
            case PROJECTS_TO_FILE_XML:
               return saveXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_TO_FILE_JSON:
               return saveJSON(PROJECTS_FILE_NAME_JSON);
            case PROJECTS_FROM_FILE_XML:
               return uploadFromXML(PROJECTS_FILE_NAME_XML);
            case PROJECTS_FROM_FILE_JSON:
               return uploadFromJSON(PROJECTS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }

    public int createProject(Scanner scanner) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        if (userService.currentUser == null) projectService.create(name, description);
        else projectService.create(name, description, userService.currentUser.getId());
        return 0;
    }


    public int updateProjectByIndex(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Long userId = userService.currentUser.getId();
        final Project project = projectService.findByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()), userId);
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not updated");
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            logger.info("Project was not updated");
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description, userId);
        logger.info("Project was updated");
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long userId = userService.currentUser.getId();
        final Project project = projectService.findById(Listener.inputIdCheckFormat(scanner.nextLine()), userId);
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not updated");
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            logger.info("Project was not updated");
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description, userId);
        logger.info("Project was updated");
        System.out.println("[OK]");
        return 0;
    }

    public int clearProject() throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT]");
        if (userService.currentUser == null) {
            projectService.clear();
            projectTaskService.clear();
        } else {
            for (Project project : projectService.findAllByUserId(userService.currentUser.getId())) {
                projectService.removeById(project.getId(), userService.currentUser.getId());
            }
        }
        logger.info("Projects were deleted.");
        System.out.println("Projects were deleted.");
        return 0;
    }

    public int removeProjectByIndex(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.removeByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()), userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project is not removed.");
            return -1;
        }
        final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
        for (final Task task : tasks) {
            projectTaskService.removeTaskFromProject(project.getId(), task.getId());
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        final List<Project> projects = projectService.removeByName(name, userService.currentUser.getId());
        if (projects == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not removed.");
            return -1;
        }
        for (Project project : projects) {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
        }
        logger.info("Project was removed.");
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("[CLEAR PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.removeById(Listener.inputIdCheckFormat(scanner.nextLine()), userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
            logger.info("Project was not removed.");
            return -1;
        }
        final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
        for (final Task task : tasks) {
            projectTaskService.removeTaskFromProject(project.getId(), task.getId());
        }
        logger.info("Project was removed.");
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    public int viewProjectByIndex(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.findByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()), userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int viewProjectByName(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("ENTER PROJECT NAME: ");
        final List<Project> projects = projectService.findByName(scanner.nextLine(), userService.currentUser.getId());
        viewProjects(projects);
        return 0;
    }

    public int viewProjectById(Scanner scanner) throws ProjectNotFoundException {
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.findById(Listener.inputIdCheckFormat(scanner.nextLine()), userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null) {
            projectList = projectService.findAll();
        } else {
            projectList = projectService.findAllByUserId(userService.currentUser.getId());
        }
        return viewProjects(projectList);

    }

    public int viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("[FAIL]");
            logger.info("PROJECTS ARE NOT FOUND.");
            return -1;
        }
        int index = 1;
        projects.sort(Comparator.comparing(Project::getName));
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        projectService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        projectService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        projectService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName) throws IOException {
        if (fileName == null|| fileName.isEmpty()) return -1;
        projectService.uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }
}
