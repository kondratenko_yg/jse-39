package ru.kondratenko.tm.observer.Listener;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static ru.kondratenko.tm.constant.TerminalConst.*;

public class ListenerTaskImpl implements Listener {
    private final TaskService taskService;

    private final UserService userService;

    private final ProjectTaskService projectTaskService;

    public ListenerTaskImpl() {
        this.userService = UserService.getInstance();
        this.taskService = TaskService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
    }

    @Override
    public int update(String param, Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException {
        switch (param) {
            case TASK_CREATE:
                return createTask(scanner);
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex(scanner);
            case TASK_VIEW_BY_ID:
                return viewTaskById(scanner);
            case TASK_VIEW_BY_NAME:
                return viewTaskByName(scanner);
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex(scanner);
            case TASK_REMOVE_BY_ID:
                return removeTaskById(scanner);
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName(scanner);
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex(scanner);
            case TASK_UPDATE_BY_ID:
                return updateTaskById(scanner);
            case TASK_LIST_BY_PROJECT_ID:
                return listTaskByProjectId(scanner);
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return addTaskToProjectByIds(scanner);
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return removeTaskFromProjectByIds(scanner);
            case TASKS_TO_FILE_XML:
                return saveXML(TASKS_FILE_NAME_XML);
            case TASKS_TO_FILE_JSON:
                return saveJSON(TASKS_FILE_NAME_JSON);
            case TASKS_FROM_FILE_XML:
                return uploadFromXML(TASKS_FILE_NAME_XML);
            case TASKS_FROM_FILE_JSON:
                return uploadFromJSON(TASKS_FILE_NAME_JSON);
            default:
                return -1;
        }
    }

    public int createTask(Scanner scanner) {
        System.out.println("[CREATE TASK]");
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        if (userService.currentUser == null) taskService.create(name, description);
        else taskService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex(Scanner scanner) throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()),userId);
        if (task == null) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description,userId);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById(Scanner scanner) throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findById(Listener.inputIdCheckFormat(scanner.nextLine()),userId);
        if (task == null) {
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        if(Listener.checkProjectName(name)){
            return -1;
        }
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description,userId);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() throws TaskNotFoundException {
        System.out.println("[CLEAR TASK]");
        if (userService.currentUser == null) {
            taskService.clear();
            projectTaskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.currentUser.getId())) {
                taskService.removeById(task.getId(),userService.currentUser.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName(Scanner scanner) throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY NAME]");
        System.out.println("ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final List<Task> task = taskService.removeByName(name,userService.currentUser.getId());
        if (task == null){
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex(Scanner scanner) throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.removeByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()),userService.currentUser.getId());
        if (task == null){
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById(Scanner scanner) throws TaskNotFoundException {
        System.out.println("[CLEAR TASK BY ID]");
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.removeById(Listener.inputIdCheckFormat(scanner.nextLine()),userService.currentUser.getId());
        if (task == null){
            System.out.println("[FAIL]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex(Scanner scanner) throws TaskNotFoundException {
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.findByIndex(Listener.inputIndexCheckFormat(scanner.nextLine()),userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskById(Scanner scanner) throws TaskNotFoundException {
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.findById(Listener.inputIdCheckFormat(scanner.nextLine()),userService.currentUser.getId());
        viewTask(task);
        return 0;
    }

    public int viewTaskByName(Scanner scanner) throws TaskNotFoundException {
        System.out.println("ENTER TASK NAME: ");
        final List<Task> tasks = taskService.findByName(scanner.nextLine(),userService.currentUser.getId());
        viewTasks(tasks);
        return 0;
    }

    public int viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return -1;
        }
        int index = 1;
        tasks.sort(Comparator.comparing(Task::getName));
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        List<Task> taskList;
        if (userService.currentUser == null) taskList = taskService.findAll();
        else taskList = taskService.findAllByUserId(userService.currentUser.getId());
        return viewTasks(taskList);
    }

    public int listTaskByProjectId(Scanner scanner) {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = Listener.inputIdCheckFormat(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        return viewTasks(tasks);
    }

    public int addTaskToProjectByIds(Scanner scanner) throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = Listener.inputIdCheckFormat(scanner.nextLine());
        System.out.println("ENTER TASK ID: ");
        final long taskId = Listener.inputIdCheckFormat(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId,userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds(Scanner scanner) {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = Listener.inputIdCheckFormat(scanner.nextLine());
        System.out.println("ENTER TASK ID: ");
        final long taskId = Listener.inputIdCheckFormat(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


    public int saveJSON(final String  fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.saveJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int saveXML(final String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.saveXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromJSON(final String  fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService.uploadFromJSON(fileName);
        System.out.println("[OK]");
        return 0;
    }

    public int uploadFromXML(final String  fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) return -1;
        taskService. uploadFromXML(fileName);
        System.out.println("[OK]");
        return 0;
    }

}

