package ru.kondratenko.tm.observer.Listener;

import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

public interface Listener {
    int update(String command, Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException;
    static int inputIndexCheckFormat(String input) {
        final int index;
        try {
            index = Integer.parseInt(input) - 1;
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return -1;
        }
        return index;
    }

    static long inputIdCheckFormat(String command) {
        final long id;
        try {
            id = Long.parseLong(command);
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

    static boolean checkProjectName(String name){
        if(Pattern.compile("^\\d").matcher(name).lookingAt()){
            System.out.println("[NAMES SHOULD NOT START FROM NUMBERS]");
            return true;
        }
        return false;
    }

}
